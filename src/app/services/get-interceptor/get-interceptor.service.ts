import { Injectable } from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import { CommonService } from 'src/app/services/common.service';

@Injectable({
  providedIn: 'root'
})
export class GetInterceptorService {

  constructor(
    private router: Router,
    private commonService: CommonService
  ) { }
  
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // this.commonService.hideSpinner();
      }
    }, (error: any) => {
      if (error['status'] === 401) {
        // this.commonService.errorMsg(error['error']['message']); 
        // this.commonService.hideSpinner();
        localStorage.clear();
        this.router.navigate(['/']);
      } 
      else if(error['status'] === 400){
        // this.commonService.hideSpinner();
        // this.commonService.errorMsg(error['error']['message']);
      }
      else if(error['status'] === 500){
        // this.commonService.hideSpinner();
        // this.commonService.errorMsg(error['error']['message']);
      }
      else if(error['status'] === 404){
        // this.commonService.hideSpinner();
        // this.commonService.errorMsg('Not Found the Path');
      }else{
        // this.commonService.hideSpinner();
        // this.commonService.errorMsg('Something went Wrong!');
      }
    }));
  }
}

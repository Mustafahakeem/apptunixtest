import { Injectable } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';


@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    public toastr: ToastrManager
  ) { }

  isLoggedIn() {
    if (localStorage.getItem('apptunix')) return true;
  }
  errorMsg(msg){
    this.toastr.errorToastr(msg);
  }

  successMsg(msg){
    this.toastr.successToastr(msg);
  }

}

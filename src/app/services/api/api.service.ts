import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'
import { map, catchError } from "rxjs/operators";
import { Observable } from 'rxjs/internal/Observable';
import { CommonService } from 'src/app/services/common.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseUrl = environment.baseUrl;

  constructor(
    private _commonService: CommonService,
    private _http: HttpClient,
    private _router: Router
  ) { }

  getRequest(endPoint, reqBody) {
    return this._http.get(`${this.baseUrl}${endPoint}`, reqBody)
    .pipe(
      catchError(this.handleError<any>('Get Request'))
    );
  }

  postRequest(endPoint, reqBody) {
    return this._http.post(`${this.baseUrl}${endPoint}`, reqBody)
    .pipe(
      catchError(this.handleError<any>('Post Request'))
    );
  }

  putRequest(endPoint, reqBody) {
    return this._http.put(`${this.baseUrl}${endPoint}`, reqBody)  
    .pipe(
      catchError(this.handleError<any>('Put Request'))
    );
  }

  deleteRequest(endPoint, reqBody) {
    return this._http.delete(`${this.baseUrl}${endPoint}`, reqBody)
    .pipe(
      catchError(this.handleError<any>('Delete Request'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      if(error['status'] == 400 || error['status'] == 401){
        // this._commonService.errorMsg(error['error']['message'])
       // localStorage.clear();
        // this._commonService.hideSpinner();
       // this._router.navigate(['/auth/login'])
      } else {
        //504
        console.error(error);
      }
      return;
    };
  }
}

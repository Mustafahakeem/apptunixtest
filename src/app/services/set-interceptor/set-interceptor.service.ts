import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpRequest, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import { CommonService } from '../common.service';

@Injectable({
  providedIn: 'root'
})
export class SetInterceptorService {

  user : any;

  constructor(
    private common : CommonService
  ) { }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // this.common.showSpinner();
    const clonedReq = this.handleRequest(req);
    return next.handle(clonedReq);
  }
  handleRequest(req: HttpRequest<any>) {
    let auth;
    if(localStorage.getItem('dxg_dating_admin_token')){
      // console.log(localStorage.getItem('dxg_dating_admin_token'))
      auth = JSON.parse(localStorage.getItem('dxg_dating_admin_token'));
    }
    let authReq;
    if ((req.method.toLowerCase() === 'post' || req.method.toLowerCase() === 'put') && req.body instanceof FormData) {
      authReq = req.clone({
        headers: new HttpHeaders({
          Authorization: auth ? 'Bearer '+ auth : ''
        })
      });
    } else {
      authReq = req.clone({
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: auth ? 'Bearer '+ auth : ''
        })
      });
    }
    return authReq;
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { PaginationComponent } from './pagination/pagination.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@NgModule({
  declarations: [DashboardComponent, PaginationComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    NgMultiSelectDropDownModule,
    InfiniteScrollModule
  ]
})
export class PagesModule { }

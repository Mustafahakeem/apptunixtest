import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';

// const nisPackage = require("../../package.json"); 

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  array = [];
  sum = [];
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = "";
  modalOpen = false; 
  key = 1

  // nisVersion = nisPackage.dependencies["ngx-infinite-scroll"];

  constructor(
    private api : ApiService
  ) { 
    this.appendItems(this.key, this.sum);
  }

  ngOnInit(): void {
  }
  addItems(startIndex, endIndex, _method) {
    for (let i = 0; i < this.sum.length; ++i) {
      this.array[_method]([JSON.stringify(this.sum[i]), " ", this.generateWord()].join(""));
    }
    console.log(this.array);
    
  }

  appendItems(startIndex, endIndex) {
    this.api.getRequest(`properties?page=${startIndex}`,'').subscribe((res : any)=>{
      if(res){
        this.sum = res;
        this.addItems(startIndex, endIndex, "push");
      }
    })
  }

  prependItems(startIndex, endIndex) {
    this.addItems(startIndex, endIndex, "unshift");
  }

  onScrollDown(ev) {
    console.log("scrolled down!!", ev);

    // add another 20 items
    const start = this.key + 1;
    this.key += 1;
    this.appendItems(start, this.key);

    this.direction = "down";
  }

  onUp(ev) {
    console.log("scrolled up!", ev);
    const start = this.key;
    this.key += 1;
    this.prependItems(start, this.key); 

    this.direction = "up";
  }
  generateWord() {
    // return chance.word();
  }

  toggleModal() {
    this.modalOpen = !this.modalOpen;
  }
}

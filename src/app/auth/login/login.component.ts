import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder , FormControl ,FormGroup , Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api/api.service'
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  isSubmitted : boolean = false;
  remember: boolean = false;

  constructor(
    private router : Router,
    private fb : FormBuilder,
    private apiService : ApiService,
    private common : CommonService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      email : ["", [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$")]],
      password : ["", [Validators.required]]
    });
    if (sessionStorage.getItem("remember")) {
      let data = JSON.parse(sessionStorage.getItem("remember"));
      this.remember = true;
      this.form.controls['email'].patchValue(data.email);
      this.form.controls['password'].patchValue(data.password);
    }
  }


  submit(){
    if(this.form.valid){
      this.apiService.postRequest('login', this.form.value).subscribe(res=>{
        if(res['success']==true){ 
          this.common.successMsg("Logged In!");
          localStorage.setItem("apptunix",JSON.stringify(res['data']['token']));
          if (this.remember) {
            let body = this.form.value;
            sessionStorage.setItem(
              "remember",
              JSON.stringify(body)
            );
          } else {
            sessionStorage.clear();
          }
          this.router.navigateByUrl("/dashboard"); 
        }else{
          this.common.errorMsg(res['message']);
        }
      })
    }else{
      this.isSubmitted = true;
    }
    
  }
  valueChange(event){
    this.remember = event.checked; 
  }
}

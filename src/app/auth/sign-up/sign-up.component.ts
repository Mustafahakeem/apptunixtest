import { Component, OnInit } from '@angular/core';
import { FormBuilder , FormControl ,FormGroup , Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service'
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  form: FormGroup;
  isSubmitted : boolean = false;

  constructor(
    private fb : FormBuilder,
    private router : Router,
    private apiService : ApiService,
    private common : CommonService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      email : ["", [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$")]],
      password : ["", [Validators.required]]
    });
  }

  submit(){
    if(this.form.valid){
      this.apiService.postRequest('signup', this.form.value).subscribe(res=>{
        if(res['success']==true){ 
          this.common.successMsg("Successfull!");
          this.router.navigateByUrl("/auth"); 
        }else{
          this.common.errorMsg(res['message']);
        }
      }) 
    }else{
      this.isSubmitted = true;
    }
    
  }
}
